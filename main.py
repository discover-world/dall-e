import argparse
import torch
from train.vae import VAE
from train.dalle import DallE_
from utils.coco_data_process import prepare_image_coco
from utils.cub_data_process import prepare_cub_data

parser = argparse.ArgumentParser(description='train VAE for DALLE-pytorch')
parser.add_argument('--train', type=str, default="", help='path to imageFolder (default: ./imagedata')
parser.add_argument('--predict', type=str, default="", help='path to imageFolder (default: ./imagedata')
parser.add_argument('--prepare_data', type=str, default="", help='path to imageFolder (default: ./imagedata')
parser.add_argument('--batch_size', type=int, default=24, help='batch size for training (default: 24)')
parser.add_argument('--data_path', type=str, default="./imagedata", help='path to imageFolder (default: ./imagedata')
parser.add_argument('--root_output_folder', type=str, default="./output",
                    help='path to imageFolder (default: ./imagedata')
parser.add_argument('--img_size', type=int, default=256, help='image size for training (default: 256)')
parser.add_argument('--n_epochs', type=int, default=500, help='number of epochs (default: 500)')
parser.add_argument('--lr', type=float, default=1e-4, help='learning rate (default: 1e-4)')
parser.add_argument('--temp_sched', action='store_true', default=False, help='use temperature scheduling')
parser.add_argument('--temperature', type=float, default=0.9, help='vae temperature (default: 0.9)')
parser.add_argument('--name', type=str, default="vae", help='experiment name')
parser.add_argument('--load_fn', type=str, default="", help='name for pretrained VAE when continuing training')
parser.add_argument('--load_from_epoch', type=int, default=-1, help='name for pretrained VAE when continuing training')
parser.add_argument('--save_interval', type=int, default=0, help='name for pretrained VAE when continuing training')
parser.add_argument('--dataset_name', type=str, default="", help='name for pretrained VAE when continuing training')
parser.add_argument('--start_epoch', type=int, default=0,
                    help='start epoch numbering for continuing training (default: 0)')
parser.add_argument('--vae_epoch', type=int, default=-1,
                    help='start epoch numbering for continuing training (default: 0)')
parser.add_argument('--clip', type=float, default=0, help='clip weights, 0 = no clipping (default: 0)')
parser.add_argument('--multi_gpus', type=bool, default=False, help='clip weights, 0 = no clipping (default: 0)')
opt = parser.parse_args()


def setup_model():
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    vae_model = VAE(
        # vae options
        img_size=opt.img_size,
        num_layers=3,
        num_tokens=512,
        codebook_dim=512,
        hidden_dim=64,
        num_resnet_blocks=0,
        channels=3,
        temperature=opt.temperature,
        # train options
        device=device,
        data_path=opt.data_path,
        dataset_name=opt.dataset_name,
        load_fn=opt.load_fn,
        load_from_epoch=opt.vae_epoch,
        lr=opt.lr,
        temperature_scheduling=opt.temp_sched,
        start_epoch=opt.start_epoch,
        n_epochs=opt.n_epochs,
        log_interval=10,
        save_interval=opt.save_interval,
        batch_size=opt.batch_size,
        clip=opt.clip,
        root_output_folder=opt.root_output_folder,
        # multi_gpus=opt.multi_gpus,
    )
    return vae_model, DallE_(
        # vae options
        dim=256,  # 512,
        num_text_tokens=10000,  # vocab size for text
        text_seq_len=256,  # text sequence length
        depth=6,  # should be 64
        heads=8,  # attention heads
        dim_head=64,  # attention head dimension
        attn_dropout=0.1,  # attention dropout
        ff_dropout=0.1,  # feedforward dropout
        # train options
        device=device,
        data_path=opt.data_path,
        dataset_name=opt.dataset_name,
        load_fn=opt.load_fn,
        load_from_epoch=opt.load_from_epoch,
        lr=opt.lr,
        start_epoch=opt.start_epoch,
        n_epochs=opt.n_epochs,
        log_interval=10,
        save_interval=opt.save_interval,
        batch_size=opt.batch_size,
        root_output_folder=opt.root_output_folder,
        img_size=opt.img_size,
        vae=vae_model,
        multi_gpus=opt.multi_gpus,
    )


if __name__ == '__main__':

    if opt.train != "":
        if opt.train == "vae":
            vae, _ = setup_model()
            vae.load_model()
            vae.train()
        elif opt.train == "dalle":
            vae, dalle = setup_model()
            vae.load_model()
            dalle.load_model()
            dalle.train()

    elif opt.predict != "":
        if opt.predict == "dalle":
            vae, dalle = setup_model()
            vae.load_model()
            dalle.load_model()
            dalle.predict()
    elif opt.prepare_data != "":
        if opt.prepare_data == "coco_dog":
            prepare_image_coco(name=opt.prepare_data, img_size=opt.img_size)
        if opt.prepare_data == "coco_dog_cat":
            prepare_image_coco(name=opt.prepare_data, img_size=opt.img_size, cats=['dog', 'cat'])
        if opt.prepare_data == "cub":
            prepare_cub_data(img_size=opt.img_size)
