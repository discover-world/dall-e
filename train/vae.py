import os
import torch
from torch import nn, optim
from torchvision import datasets, transforms
from torchvision import utils
from torch.utils.data import DataLoader
import torch.nn.functional as F
from dalle_pytorch import DiscreteVAE
import datetime
from .model_base import ModelBase
import time

class VAE(ModelBase):
    def __init__(
            self,
            device=None,
            data_path="",
            dataset_name="",
            num_tokens=512,
            codebook_dim=512,
            num_layers=3,
            num_resnet_blocks=0,
            hidden_dim=64,
            channels=3,
            temperature=0.9,
            load_fn="",
            load_from_epoch=-1,
            lr=1e-4,
            temperature_scheduling=False,
            start_epoch=0,
            n_epochs=100,
            log_interval=10,
            save_interval=0,
            img_size=256,
            batch_size=24,
            clip=0.,
            root_output_folder="output",
            multi_gpus=False
    ):
        super().__init__(
            name="vae",
            dataset_name=dataset_name,
            save_interval=save_interval,
            n_epochs=n_epochs,
            root_output_folder=root_output_folder,
            start_epoch=start_epoch,
            log_interval=log_interval,
            batch_size=batch_size,
            lr=lr,
            data_path=data_path,
            img_size=img_size,
            device=device,
            load_fn=load_fn,
            load_from_epoch=load_from_epoch,
            multi_gpus=multi_gpus,
        )

        self.clip = clip
        self.temperature_scheduling = temperature_scheduling
        self.temperature = temperature

        # self.model = DiscreteVAE(
        #     image_size=self.img_size,
        #     num_tokens=num_tokens,
        #     codebook_dim=codebook_dim,
        #     num_layers=num_layers,
        #     num_resnet_blocks=num_resnet_blocks,
        #     hidden_dim=hidden_dim,
        #     channels=channels,
        #     temperature=self.temperature,
        # )
        #
        self.model = DiscreteVAE(
            image_size=self.img_size,
            # temperature=self.temperature,
            # num_layers=3,
            # channels=3,
            # num_tokens=2048,
            # codebook_dim=256,
            # hidden_dim=128,
            # num_layers=2,
            # num_tokens=4096,
            # codebook_dim=1024,
            # hidden_dim=256,

            # num_layers=3,
            # num_tokens=8192,
            # codebook_dim=1024,
            # hidden_dim=64,
            # num_resnet_blocks=1,

            num_layers=3,
            channels=3,
            num_tokens=2048,
            codebook_dim=256,
            hidden_dim=128,
            temperature=0.9
        )

        # self.load_model(epoch=load_from_epoch, load_fn=load_fn)

        if self.multi_gpus:
            print("{0} is multiple gpu".format(self.name))
            self.model = torch.nn.DataParallel(self.model)

    def get_model(self):
        return self.model

    def clamp_weights(self, m):
        if type(m) != nn.BatchNorm2d and type(m) != nn.Sequential:
            for p in m.parameters():
                p.data.clamp_(-self.clip, self.clip)

    def before_train(self):
        if not os.path.exists(self.output_folder):
            os.makedirs(self.output_folder)

    def train(self):
        optimizer = optim.Adam(self.model.parameters(), lr=self.lr)

        t = transforms.Compose([
            transforms.Resize(self.img_size),
            transforms.ToTensor(),
            # transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))  # (0.267, 0.233, 0.234))
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])
        ])

        train_set = datasets.ImageFolder(self.data_path, transform=t, target_transform=None)
        train_loader = DataLoader(dataset=train_set, num_workers=1, batch_size=self.batch_size, shuffle=True)

        dk = 0
        if self.temperature_scheduling:
            self.model.temperature = self.temperature
            dk = 0.7 ** (1 / len(train_loader))
            print('Scale Factor:', dk)

        self.before_train()

        for epoch in range(self.start_epoch, self.start_epoch + self.n_epochs):

            train_loss = 0
            images = []
            recons = []
            start_time_epoch = time.time()
            start_time_batch = time.time()

            for batch_idx, (images, _) in enumerate(train_loader):
                images = images.to(self.device)
                recons = self.model(images)
                loss = F.smooth_l1_loss(images, recons) + F.mse_loss(images, recons)

                optimizer.zero_grad()
                loss.backward()
                train_loss += loss.item()
                optimizer.step()

                if self.clip > 0:
                    self.model.apply(self.clamp_weights)

                if batch_idx % self.log_interval == 0:

                    percent = 100. * batch_idx / len(train_loader)
                    if percent == 0:
                        estimated_time = 0
                    else:
                        estimated_time = (time.time() - start_time_batch) / (batch_idx / len(train_loader))

                    print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.8f} time: {} {}'.format(
                        epoch, batch_idx * len(images), len(train_loader.dataset),
                               percent,
                               loss.item() / len(images), time.time() - start_time_batch, estimated_time))

            if self.temperature_scheduling:
                self.model.temperature *= dk
                print("Current temperature: ", self.temperature)

            print('====> Epoch: {} Average loss: {:.8f} time: {}'.format(epoch, train_loss / len(train_loader.dataset),
                                                                         time.time() - start_time_epoch))
            if epoch % self.save_interval == 0 or epoch == (self.start_epoch + self.n_epochs - 1):
                k = 8
                with torch.no_grad():
                    codes = self.model.get_codebook_indices(images)
                    imgx = self.model.decode(codes)
                grid = torch.cat([images[:k], recons[:k], imgx[:k]])
                self.save_image(grid, epoch)
                self.save_model(epoch)
