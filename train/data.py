import os
import random
from .vocabulary import Vocabulary, tokenizer


class DataInfo:
    def __init__(self, path="./"):
        self.path = path

        with open(os.path.join(self.path, "od-captionsonly.txt"), "r") as lf:
            self.vocab = Vocabulary("captions")
            captions = []

            for lin in lf:
                captions.append(lin)

            for caption in captions:
                self.vocab.add_sentence(caption)

    def word2vector(self, text):
        tokens = tokenizer(text)
        codes = []
        for t in tokens:
            if t == "":
                continue
            codes.append(self.vocab.to_index(t))
        return codes

    def get_data(self):
        ds = []

        with open(os.path.join(self.path, "od-captions.txt"), "r") as lf:
            for lin in lf:
                (fn, txt) = lin.split(":", 1)
                fn = fn.strip()
                txt = txt.strip()
                ds.append((fn, self.word2vector(txt)))

        random.shuffle(ds)
        print(len(ds))

        return ds


if __name__ == '__main__':
    data = DataInfo().get_data()
    print(data[:2])
