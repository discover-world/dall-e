from .data import DataInfo


class ImageCaptions:
    def __init__(self, data, batch_size=4):
        self.data = data
        self.len = len(data)
        self.index = 0
        self.end = False
        self.batch_size = batch_size

    def __iter__(self):
        return self

    def __next__(self):
        if self.end:
            self.index = 0
            raise StopIteration
        i_data = []
        c_data = []
        for i in range(0, self.batch_size):
            i_data.append(self.data[self.index][0])
            c_tokens = [0] * 256  # fill to match text_seq_len
            c_tokens_ = self.data[self.index][1]
            c_tokens[:len(c_tokens_)] = c_tokens_
            c_data.append(c_tokens)
            self.index += 1
            if self.index == self.len:
                self.end = True
                break
        return i_data, c_data


if __name__ == '__main__':
    data = DataInfo().get_data()
    dset = ImageCaptions(data)
    for i, c in dset:
        print(i)
        print(c)
