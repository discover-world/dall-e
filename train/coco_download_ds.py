from pycocotools.coco import COCO
import requests
import json
import os

captions_dict = {}

with open('annotations/captions_train2017.json', 'r') as f:
    array = json.load(f)
    for info in array['annotations']:
        captions_dict[info['image_id']] = info['caption']

coco = COCO('./annotations/instances_train2017.json')
# cats = coco.loadCats(coco.getCatIds())
# nms = [cat['name'] for cat in cats]
# print('COCO categories: \n{}\n'.format(' '.join(nms)))

catIds = coco.getCatIds(catNms=['dog'])
imgIds = coco.getImgIds(catIds=catIds)
images = coco.loadImgs(imgIds)

print(len(images))
with open("od-captions.txt", 'w') as out:
    with open("od-captionsonly.txt", 'w') as outco:
        c = 0
        for im in images:
            print("im: {0}/{1} {2}".format(c, len(images), im['coco_url']))
            if im['id'] not in captions_dict:
                print("img {0} doesn't have captions", im['id'])
                continue

            caption = captions_dict[im['id']].replace('\n', '')

            image_id_str = os.path.splitext(im['file_name'])[0]

            img_path = 'downloaded_images/' + im['file_name']
            if os.path.isfile(img_path):
                img_data = requests.get(im['coco_url']).content
                with open(img_path, 'wb') as handler:
                    handler.write(img_data)

            out.write(str(image_id_str) + " : " + caption + '\n')
            outco.write(caption + '\n')
            c = c + 1
