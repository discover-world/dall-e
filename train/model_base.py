import os
import torch
from torchvision import utils


class ModelBase:
    def __init__(
            self,
            device=None,
            name="",
            dataset_name="",
            save_interval=0,
            n_epochs=100,
            root_output_folder="output",
            start_epoch=0,
            log_interval=10,
            img_size=256,
            batch_size=24,
            lr=1e-4,
            data_path="",
            load_fn="",
            load_from_epoch=-1,
            multi_gpus=False,
    ):
        self.model = None
        self.name = name
        self.n_epochs = n_epochs
        self.start_epoch = start_epoch
        self.log_interval = log_interval
        self.img_size = img_size
        self.batch_size = batch_size
        self.lr = lr
        self.data_path = data_path
        self.device = device
        self.load_fn = load_fn
        self.load_from_epoch = load_from_epoch
        self.dataset_name = dataset_name
        self.multi_gpus = multi_gpus
        if dataset_name == "":
            self.dataset_name = os.path.basename(os.path.normpath(self.data_path))

        # root > dataset_name > size > datetime
        # datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
        print(root_output_folder)
        self.output_folder = os.path.join(root_output_folder, self.dataset_name)

        if save_interval == 0:
            self.save_interval = int(n_epochs / 10)
        else:
            self.save_interval = save_interval

    def load_model(self) -> None:
        epoch = self.load_from_epoch
        load_fn = self.load_fn
        if epoch != -1:
            path = self.get_model_save_path(epoch)
        else:
            path = load_fn

        if path != "":
            print("Loading {0} from {1}".format(self.name, path))
            model_dict = torch.load(path)
            self.model.load_state_dict(model_dict)

        self.model.to(self.device)

    def get_model_save_path(self, epoch: int) -> str:
        return "{0}/models/{1}_{2}_{3}.pth".format(self.output_folder, self.name, self.dataset_name, epoch)

    def get_image_save_path(self, epoch: int) -> str:
        return "{0}/images/{1}_{2}_{3}.png".format(self.output_folder, self.name, self.dataset_name, epoch)

    def save_model(self, epoch: int) -> None:
        path = self.get_model_save_path(epoch)
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
        torch.save(self.model.state_dict(), path)
        print("Saved model {0}".format(path))

    def save_image(self, image: torch.Tensor, epoch: int) -> None:
        path = self.get_image_save_path(epoch)
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
        utils.save_image(image, path, normalize=True)
        print("Saved image {0}".format(path))
