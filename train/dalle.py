import torch
from dalle_pytorch import DiscreteVAE, DALLE
from torchvision.io import read_image
import torchvision.transforms as transforms
import torch.optim as optim
from torchvision.utils import save_image
from .data import DataInfo
from train.image_caption import ImageCaptions
from .vae import VAE, ModelBase
import time


class DallE_(ModelBase):
    def __init__(
            self,
            dim=256,  # 512,
            num_text_tokens=10000,  # vocab size for text
            text_seq_len=256,  # text sequence length
            depth=6,  # should be 64
            heads=8,  # attention heads
            dim_head=64,  # attention head dimension
            attn_dropout=0.1,  # attention dropout
            ff_dropout=0.1,  # feedforward dropout
            device=None,
            data_path="",
            dataset_name="",
            load_fn="",
            load_from_epoch=0,
            lr=1e-4,
            start_epoch=0,
            n_epochs=100,
            log_interval=10,
            save_interval=0,
            img_size=256,
            batch_size=24,
            root_output_folder="output",
            multi_gpus=False,
            vae: VAE = None,
    ):
        super().__init__(
            name="dalle",
            dataset_name=dataset_name,
            save_interval=save_interval,
            n_epochs=n_epochs,
            root_output_folder=root_output_folder,
            start_epoch=start_epoch,
            log_interval=log_interval,
            batch_size=batch_size,
            lr=lr,
            data_path=data_path,
            img_size=img_size,
            device=device,
            load_fn=load_fn,
            load_from_epoch=load_from_epoch,
            multi_gpus=multi_gpus,
        )

        # self.model = DALLE(
        #     vae=vae.get_model(),  # automatically infer (1) image sequence length and (2) number of image tokens
        #     dim=dim,  # 512,
        #     num_text_tokens=num_text_tokens,
        #     text_seq_len=text_seq_len,  # text sequence length
        #     depth=depth,  # should be 64
        #     heads=heads,  # attention heads
        #     dim_head=dim_head,  # attention head dimension
        #     attn_dropout=attn_dropout,  # attention dropout
        #     ff_dropout=ff_dropout  # feedforward dropout
        # )

        self.model = DALLE(
            # dim=1024,
            # vae=vae.model,  # automatically infer (1) image sequence length and (2) number of image tokens
            # num_text_tokens=10000,  # vocab size for text
            # text_seq_len=256,  # text sequence length
            # depth=6,  # should aim to be 64
            # heads=8,  # attention heads
            # dim_head=32,  # attention head dimension
            # attn_dropout=0.1,  # attention dropout
            # ff_dropout=0.1

            # dim=1024,
            # vae=vae.model,  # automatically infer (1) image sequence length and (2) number of image tokens
            # num_text_tokens=10000,  # vocab size for text
            # text_seq_len=256,  # text sequence length
            # depth=12,  # should aim to be 64
            # heads=16,  # attention heads
            # dim_head=64,  # attention head dimension
            # attn_dropout=0.1,  # attention dropout
            # ff_dropout=0.1,  # feedforward dropout,

            dim=256,  # 512,
            vae=vae.model,  # automatically infer (1) image sequence length and (2) number of image tokens
            num_text_tokens=10000,  # vocab size for text
            text_seq_len=256,  # text sequence length
            depth=6,  # should be 64
            heads=8,  # attention heads
            dim_head=64,  # attention head dimension
            attn_dropout=0.1,  # attention dropout
            ff_dropout=0.1  # feedforward dropout

        )

        if self.multi_gpus:
            print("{0} is multiple gpu".format(self.name))
            self.model = torch.nn.DataParallel(self.model)

    def train(self):
        optimizer = optim.Adam(self.model.parameters(), lr=self.lr)
        data = DataInfo(self.data_path).get_data()

        tf = transforms.Compose([
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])

        for epoch in range(self.start_epoch, self.start_epoch + self.n_epochs):
            batch_idx = 0
            train_loss = 0
            start_time_epoch = time.time()

            dset = ImageCaptions(data, batch_size=self.batch_size)
            start_time_batch = time.time()

            for i, c in dset:
                text = torch.LongTensor(c)
                images = torch.zeros(len(i), 3, self.img_size, self.img_size)
                text = text.to(self.device)

                ix = 0
                for imgfn in i:  # iterate through image paths in minibatch
                    img_t = read_image("{0}/0/{1}.jpg".format(self.data_path, imgfn)).float() / 255.
                    if img_t.size()[0] == 1:
                        continue
                    img_t = tf(img_t)  # normalize
                    images[ix, :, :, :] = img_t
                    ix += 1

                images = images.to(self.device)

                mask = torch.ones_like(text).bool().to(self.device)

                # train and optimize a single minibatch
                optimizer.zero_grad()

                # print(text.size())
                # print(images.size())
                loss = self.model(text, images, mask=mask, return_loss=True)
                train_loss += loss.item()
                loss.backward()
                optimizer.step()

                if batch_idx % self.log_interval == 0:
                    percent = 100. * batch_idx / int(round(len(data) / self.batch_size))
                    if percent == 0:
                        estimated_time = 0
                    else:
                        estimated_time = (time.time() - start_time_batch) / (batch_idx / int(round(len(data) / self.batch_size)))
                    print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f} time: {}'.format(
                        epoch, batch_idx * len(i), len(data),
                               percent,
                               loss.item() / len(i), estimated_time))

                batch_idx += 1


            print('====> Epoch: {} Average loss: {:.4f}, time: {}'.format(epoch, train_loss / len(data),
                                                                          time.time() - start_time_epoch))
            if epoch % self.save_interval == 0 or epoch == (self.start_epoch + self.n_epochs - 1):
                oimgs = self.model.generate_images(text, mask=mask)
                self.save_image(oimgs, epoch)
                self.save_model(epoch)

    def predict(self):
        data_info = DataInfo(self.data_path)

        with open('test.txt', 'r') as f:
            lines = f.readlines()
            count = 0
            for line in lines:
                line = line.strip()
                count += 1
                print("Line{}: {}".format(count, line))
                codes = data_info.word2vector(line)

                c_tokens = [0] * 256  # fill to match text_seq_len
                c_tokens[:len(codes)] = codes

                text = torch.LongTensor(codes).unsqueeze(0).to(self.device)  # a minibatch of text (numerical tokens)
                mask = torch.ones_like(text).bool().to(self.device)
                for i in range(8):
                    oimgs = self.model.generate_images(text, mask=mask)
                    save_image(oimgs, './output/predict/test_{0}_{1}.png'.format(count, i + 1), normalize=True)
# vae = DiscreteVAE(
#     image_size=opt.imageSize,
#     num_layers=3,
#     channels=3,
#     num_tokens=2048,
#     codebook_dim=256,
#     hidden_dim=128,
#     temperature=0.9
# )
