import shutil
import os


def prepare_data_folder(name: str, img_size: int) -> str:
    output_folder = "./data/{0}_{1}".format(name, img_size)

    try:
        shutil.rmtree(output_folder)
    except OSError as e:
        print("Error: %s - %s." % (e.filename, e.strerror))

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
        os.makedirs(os.path.join(output_folder, "0"))

    return output_folder
