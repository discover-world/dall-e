import argparse
import os
from PIL import Image

parser = argparse.ArgumentParser(description='train VAE for DALLE-pytorch')
parser.add_argument('--dataPath', type=str, default="./imagedata", help='path to imageFolder (default: ./imagedata')
parser.add_argument('--imageSize', type=int, default=256, help='image size for training (default: 256)')
opt = parser.parse_args()

imgSize = opt.imageSize

imagePaths = []
for root, dirs, files in os.walk(opt.dataPath):
    for fname in files:
        if ".png" in fname or ".jpg" in fname:
            path = os.path.join(root, fname)
            im = Image.open(path)
            im_resized = im.resize((imgSize, imgSize))
            im_resized.save(path)
            print("Resizing: " + fname)
