import os
from .base import prepare_data_folder
from PIL import Image


def prepare_cub_data(name="cub", img_size=256):
    output_folder = prepare_data_folder(name, img_size)
    description_path = "/home/nc/Downloads/birds/text_c10"
    images_path = "/home/nc/Downloads/CUB_200_2011/CUB_200_2011/images"

    description_path = os.environ['DESCRIPTION_PATH']
    images_path = os.environ['IMAGE_PATH']

    folder_description_cats = [name for name in os.listdir(description_path) if
                               os.path.isdir(os.path.join(description_path, name))]
    folder_images_cats = [name for name in os.listdir(images_path) if os.path.isdir(os.path.join(images_path, name))]

    with open(os.path.join(output_folder, "od-captions.txt"), 'w') as out:
        with open(os.path.join(output_folder, "od-captionsonly.txt"), 'w') as outco:
            for cat in folder_description_cats:
                if not os.path.exists(os.path.join(images_path, cat)):
                    print("{0} does not exist".format(os.path.join(images_path, cat)))
                    continue

                folder_description = os.path.join(description_path, cat)

                description_files = [name for name in os.listdir(folder_description) if name.endswith(".txt")]

                for df in description_files:
                    src = os.path.join(images_path, cat, df.replace(".txt", ".jpg"))
                    if not os.path.exists(src):
                        print("{0} does not exist".format(src))
                        continue
                    with open(os.path.join(folder_description, df), "r") as f:
                        captions = f.readlines()
                        dst_img_name = df.replace(".txt", ".jpg")
                        dst = os.path.join(output_folder, "0", dst_img_name)
                        image = Image.open(src)
                        im_resized = image.resize((img_size, img_size))
                        im_resized.save(dst)
                        print("{0} -> {1}".format(src, dst))

                        for caption in captions:
                            caption = caption.replace("\n", "")
                            image_id_str = os.path.splitext(dst_img_name)[0]
                            out.write(str(image_id_str) + " : " + caption + '\n')
                            outco.write(caption + '\n')
