from pycocotools.coco import COCO
import requests
import json
import os
from PIL import Image
import shutil


def prepare_image_coco(name="dog", img_size=64, cats=['dog']):
    captions_dict = {}
    output_folder = "./data/{0}_{1}".format(name, img_size)

    try:
        shutil.rmtree(output_folder)
    except OSError as e:
        print("Error: %s - %s." % (e.filename, e.strerror))

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
        os.makedirs(os.path.join(output_folder, "0"))

    with open('annotations/captions_train2014.json', 'r') as f:
        array = json.load(f)
        for info in array['annotations']:
            captions_dict[info['image_id']] = info['caption']

    coco = COCO('./annotations/instances_train2014.json')
    # cats = coco.loadCats(coco.getCatIds())
    # nms = [cat['name'] for cat in cats]
    # print('COCO categories: \n{}\n'.format(' '.join(nms)))

    catIds = coco.getCatIds(catNms=cats)
    imgIds = coco.getImgIds(catIds=catIds)
    images = coco.loadImgs(imgIds)

    print(len(images))
    with open(os.path.join(output_folder, "od-captions.txt"), 'w') as out:
        with open(os.path.join(output_folder, "od-captionsonly.txt"), 'w') as outco:
            c = 0
            for im in images:
                # print(im)
                # print("im: {0}/{1} {2}".format(c, len(images), im['coco_url']))
                if im['id'] not in captions_dict:
                    print("img {0} doesn't have captions", im['id'])
                    continue

                caption = captions_dict[im['id']].replace('\n', '')
                src = os.path.join("train2014", im["file_name"])
                dst = os.path.join(output_folder, "0", im["file_name"])
                print("{0} -> {1}".format(src, dst))

                image = Image.open(src)
                im_resized = image.resize((img_size, img_size))
                im_resized.save(dst)

                image_id_str = os.path.splitext(im['file_name'])[0]
                out.write(str(image_id_str) + " : " + caption + '\n')
                outco.write(caption + '\n')
                c = c + 1
