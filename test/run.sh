#!/bin/bash

cat <<EOF >test.txt
A man standing next to another man with a skateboard.
A cat stares at a dog on TV.
A dog laying down and covering its eyes on a concrete platform above water.
Small, happy dog sniffing and exploring an unmowed lawn.
A dog sitting on the roof of a building next to a satellite dish and a surfboard.
two dogs wearing jackets standing in a yard
Dog standing by a computer filled with magnets.
Dog laying under a blanket on top of a blanket
A small dog eating out of a bowl on the floor.
A woman lying in a bed with dogs and a laptop.
A Papillon lies on a blanket with a toy shaped like a piece of cake.
A couple and two dogs walking on the beach.
A man is using his large laptop in the living room.
a couple of people that are walking around
A dog in an action shot, catching a frisbee.
A couple of women sitting on a bench with a dog.
Young boys on a couch with their stuffed animals and a laptop computer
A puppy all dressed up with a nice red tie with hearts on it.
A dog rests his head on a pillow
A cute little dog biting on something a person is holding.
A city sidewalk with people walking and sitting on benches.
EOF

scp -r test.txt ai-lab-3:/home/acworks/project/DALLE-pytorch/coco

ssh ai-lab-3 "bash -s" <<EOF
  cd /home/acworks/project/DALLE-pytorch
  source ./venv/bin/activate
  cd /home/acworks/project/DALLE-pytorch/coco
  export CUDA_VISIBLE_DEVICES=3
  python genDALLE.py --imageSize=256 --vae 456 --dalle 108
  $(< "$script")
EOF

scp -r ai-lab-3:/home/acworks/project/DALLE-pytorch/coco/test .